def sumar(num1: int, num2: int) -> int:
    return num1 + num2


def restar(num1: int, num2: int) -> int:
    return num1 - num2

print("Calculamos: ")
print("Suma 1 + 2 = ", sumar(1,2))
print("Suma 3 + 4 = ", sumar(3,4))
print("Resta 6 - 5 = ", restar(6,5))
print("Resta 8 - 7 = ", restar(8,7))

